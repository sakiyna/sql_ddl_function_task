CREATE VIEW sales_revenue_by_category_qtr AS
SELECT 
    c.name AS category,
    SUM(p.amount) AS total_revenue
FROM 
    film f
JOIN 
    inventory i ON f.film_id = i.film_id
JOIN 
    rental r ON i.inventory_id = r.inventory_id
JOIN 
    payment p ON r.rental_id = p.rental_id
JOIN 
    film_category fc ON f.film_id = fc.film_id
JOIN 
    category c ON fc.category_id = c.category_id
WHERE 
    r.rental_date >= date_trunc('quarter', current_date)
GROUP BY 
    c.name
HAVING 
    SUM(p.amount) > 0;

   
-- Create Function:
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(quarter integer)
RETURNS TABLE (category varchar, total_revenue numeric) AS $$
BEGIN
    RETURN QUERY
    SELECT 
        c.name AS category,
        SUM(p.amount) AS total_revenue
    FROM 
        film f
    JOIN 
        inventory i ON f.film_id = i.film_id
    JOIN 
        rental r ON i.inventory_id = r.inventory_id
    JOIN 
        payment p ON r.rental_id = p.rental_id
    JOIN 
        film_category fc ON f.film_id = fc.film_id
    JOIN 
        category c ON fc.category_id = c.category_id
    WHERE 
        EXTRACT(quarter FROM r.rental_date) = quarter
    GROUP BY 
        c.name
    HAVING 
        SUM(p.amount) > 0;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION new_movie(movie_title varchar)
RETURNS void AS $$
DECLARE
    lang_id integer;
BEGIN
    SELECT language_id INTO lang_id FROM language WHERE name = 'Klingon';
    
    IF NOT FOUND THEN
        RAISE EXCEPTION 'Language Klingon does not exist in the language table';
    END IF;
    
    INSERT INTO film (title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (movie_title, 4.99, 3, 19.99, EXTRACT(year FROM current_date)::int, lang_id);
    
    RAISE NOTICE 'New movie "%"', movie_title;
END;
$$ LANGUAGE plpgsql;

